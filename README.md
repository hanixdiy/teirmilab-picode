#TeirmiLab Python Code
=========

Thermometer for laboratory

Thermometer based on Python and Raspberry Pi.
The thermometer allows to show a temperature in different measurement units and has alarm capabilities in order to notify when a temperature is reached.
It can be expanded to become a temperature data logger and remote capabilities (monitor the temperature remotely) or collect data remotely.
Other functionalities can be added in future.
This repository contains the official release of the TeirmiLab python code to be deployed on TeirmiLab HW.
For schematics/MSP430 RpOF/details/experimental code/datasheets/information look on the repository on GitHub : TeirmiLab
(https://github.com/TheFwGuy/TeirmiLab)

There are some articles on my blog about this project :

- http://hanixdiy.blogspot.com/2014/10/teirmilab-designing-laboratory.html
- http://hanixdiy.blogspot.com/2017/09/working-on-teirmilab.html
- http://hanixdiy.blogspot.com/2015/01/teirmilab-use.html
- http://hanixdiy.blogspot.com/2014/12/teirmilab-raspberry-pi-prototype.html
- http://hanixdiy.blogspot.com/2014/10/raspberry-pi-connecting-traditional-lcd.html
