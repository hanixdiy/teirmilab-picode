#!/usr/bin/python
##
# @file teirmilab.py
# @author Stefano Bodini
# @date October 2014
# @brief TeirmiLab
# Program to read a DS18S20 sensor and display the temperature on a RGB
# LCD display connected to an MCP23017 GPIO extender.
# In order to use the DS18B20 a different library is necessary.
# Use the Adafruit W1ThermSensor
#
# GPIO usage
#
# Raspberry GPIO    MCP GPIO        Direction   Description
#                       GPA0            Output      LCD
#                       GPA1            Output      LCD
#                       GPA2            Output      LCD
#                       GPA3            Output      LCD
#                       GPA4            Output      LCD
#                       GPA5            Output      LCD
#                       GPA6            Output      LCD
#                       GPA7            Output      LCD
#           
#                       GPB0            Output      LCD
#                       GPB1        
#                       GPB2        
#                       GPB3        
#                       GPB4            Input           Keyboard 1
#                       GPB5            Input           Keyboard 2
#                       GPB6            Input           Keyboard 3
#                       GPB7            Input           Keyboard 4
#           
#   GPIO4                               Input/Output    1Wire protocol
#   GPIO17                          Input           Shutdown input
#   GPIO18                          Output      Shutdown feedback
#   GPIO22                          Output      Buzzer
#   GPIO23                          Input       Encoder A
#   GPIO24                          Input       Encoder B
#   GPIO25                          Input       Encoder switch
#
import threading
import math
import time
import subprocess
import os
import glob
import sys

import Adafruit_CharLCD as LCD
import Adafruit_GPIO.MCP230xx as MCP
import Adafruit_GPIO as aGPIO
import RPi.GPIO as rGPIO

import pygame

from w1thermsensor import W1ThermSensor
from sys import argv
from os.path import exists

#script, filename = argv

############################################################################
############################################################################
############    FUNCTIONS    ###############################################
############################################################################
############################################################################

#----------------------------------------------------------------------
# This part is created from the rotary_encoder.py 
# from https://github.com/guyc/py-gaugette
# Guy Carpenter, Clearwater Software
#
# This is a class for reading quadrature rotary encoders
# like the PEC11 Series available from Adafruit:
#   http://www.adafruit.com/products/377
# The datasheet for this encoder is here:
#   http://www.adafruit.com/datasheets/pec11.pdf
#
# This library expects the common pin C to be connected
# to ground.  Pins A and B will have their pull-up resistor
# pulled high.
# 
# Usage:
#
#     import gaugette.rotary_encoder
#     A_PIN = 7  # use wiring pin numbers here
#     B_PIN = 9
#     encoder = gaugette.rotary_encoder.RotaryEncoder(A_PIN, B_PIN)
#     while 1:
#       delta = encoder.delta() # returns 0,1,or -1
#       if delta!=0:
#         print delta


class RotaryEncoder:
    #----------------------------------------------------------------------
    # Pass the wiring pin numbers here.  See:
    #  https://projects.drogon.net/raspberry-pi/wiringpi2/pins/
    #----------------------------------------------------------------------
    def __init__(self, a_pin, b_pin):
        self.a_pin = a_pin
        self.b_pin = b_pin
        
        self.gpio = rGPIO
        
        self.gpio.setup(self.a_pin, self.gpio.IN, self.gpio.PUD_UP)
        self.gpio.setup(self.b_pin, self.gpio.IN, self.gpio.PUD_UP)
        
        self.last_delta = 0
        self.r_seq = self.rotation_sequence()
        
        # steps_per_cycle and remainder are only used in get_cycles which
        # returns a coarse-granularity step count.  By default
        # steps_per_cycle is 4 as there are 4 steps per
        # detent on my encoder, and get_cycles() will return -1 or 1
        # for each full detent step.
        self.steps_per_cycle = 2
        self.remainder = 0
        
        # Gets the 2-bit rotation state of the current position
        # This is deprecated - we now use rotation_sequence instead.
    
    def rotation_state(self):
        a_state = self.gpio.input(self.a_pin)
        b_state = self.gpio.input(self.b_pin)
        r_state = a_state | b_state << 1
        return r_state
        
    # Returns the quadrature encoder state converted into
    # a numerical sequence 0,1,2,3,0,1,2,3...
    #    
    # Turning the encoder clockwise generates these
    # values for switches B and A:
    #  B A
    #  0 0
    #  0 1
    #  1 1
    #  1 0 
    # We convert these to an ordinal sequence number by returning
    #   seq = (A ^ B) | B << 2
    # 
    def rotation_sequence(self):
        a_state = self.gpio.input(self.a_pin)
        b_state = self.gpio.input(self.b_pin)

        r_seq = (a_state ^ b_state) | b_state << 1
        return r_seq
        
    # Returns offset values of -2,-1,0,1,2
    def get_delta(self):
        delta = 0
        r_seq = self.rotation_sequence()
        
        if r_seq != self.r_seq: 
            delta = (r_seq - self.r_seq) % 4
            if delta==3:
                delta = -1
            elif delta==2:
                delta = int(math.copysign(delta, self.last_delta))  # same direction as previous, 2 steps
            
            self.last_delta = delta
            self.r_seq = r_seq

        return -delta
        
    # get_cycles returns a scaled down step count to match (for example)
    # the detents on an encoder switch.  If you have 4 delta steps between
    # each detent, and you want to count only full detent steps, use
    # get_cycles() instead of get_delta().  It returns -1, 0 or 1.  If
    # you have 2 steps per detent, set encoder.steps_per_cycle to 2
    # before you call this method.
    def get_cycles(self):
        # python negative integers do not behave like they do in C.
        #   -1 // 2 = -1 (not 0)
        #   -1 % 2 =  1 (not -1)
        # // is integer division operator.  Note the behaviour of the / operator
        # when used on integers changed between python 2 and 3. 
        # See http://www.python.org/dev/peps/pep-0238/
        self.remainder += self.get_delta() 
        cycles = self.remainder // self.steps_per_cycle
        self.remainder %= self.steps_per_cycle # remainder always remains positive
        return cycles
        
    class Worker(threading.Thread):
        def __init__(self, a_pin, b_pin):
            threading.Thread.__init__(self)
            self.lock = threading.Lock()
            self.encoder = RotaryEncoder(a_pin, b_pin)
            self.daemon = True
            self.delta = 0
            
        def run(self):
            while True:
                delta = self.encoder.get_cycles()
#               delta = self.encoder.get_delta()
                with self.lock:
                    self.delta += delta
                time.sleep(0.001)
            
        def get_delta(self):
            # revisit - should use locking
            with self.lock:
                delta = self.delta
                self.delta = 0
            return delta

##
# @fn read_shutdown
# @brief  Monitor the GPIO17 and when is going 1, start shutdwon
#
def read_shutdown():
    if rGPIO.input(SHUTDOWN_INPUT) == 1:
        time.sleep(0.1) # Wait to be sure is not a spike
        if rGPIO.input(SHUTDOWN_INPUT) == 1:
            if LCD_RGB == 1:
                lcd.set_color(0.0, 0.0, 1.0)

            lcd.clear()
            lcd.message('   TeirmiLab    \n   SHUTDOWN  ')
            os.system('sudo shutdown -h now')
            while True: # Doing nothing - dead end
                count = 0

##
# @fn read_switch
# @brief  Read the encoder switch
# The function return the value to 1 when the pushbutton is pressed and
# released - it uses the global button 1 (btnMode)
# The push button is pulled up, the physical reading is 0 when pressed
#
def read_switch():
    global btnMode
    
    btnMode = 0

    if rGPIO.input(ENCODER_SWITCH) == 0:
        while rGPIO.input(ENCODER_SWITCH) == 0:
            btnMode = 0
        if rGPIO.input(ENCODER_SWITCH) == 1:
            btnMode = 1

##
# @fn read_encoder
# @brief  Read the encoder
#
def read_encoder():
    global loc_alarm
    global loc_offset
    global tmp_temper_unit
    global alarmEnable
    global executeReset

    delta = encoder.get_delta()
    
# DEBUG print   
    if delta!=0:
        print "rotate %d" % delta

        if state_machine == STM_SETUNIT:
            if delta >= 1:
                tmp_temper_unit += 1
                if tmp_temper_unit > TUN_KEV:
                    tmp_temper_unit = TUN_CEL
            elif delta <= -1:
                tmp_temper_unit -= 1
                if tmp_temper_unit < TUN_CEL:
                    tmp_temper_unit = TUN_KEV

        elif state_machine == STM_ENALARM:
            if delta <> 0:
                if alarmEnable == False:
                    alarmEnable = True
                else:
                    alarmEnable = False

        elif state_machine == STM_RESET:
            if delta <> 0:
                if executeReset == False:
                    executeReset = True
                else:
                    executeReset = False

        elif state_machine == STM_SETALARM:
            if delta <> 0:
                loc_alarm += delta * .125
            
                if temper_unit == TUN_CEL:
                    if loc_alarm > 120:
                        loc_alarm = 120
                    elif loc_alarm <= -40:
                        loc_alarm = -40
                elif temper_unit == TUN_FAR:
                    if loc_alarm > 248:
                        loc_alarm = 248
                    elif loc_alarm <= -40:
                        loc_alarm = -40
                elif temper_unit == TUN_KEV:
                    if loc_alarm > 393:
                        loc_alarm = 393
                    elif loc_alarm <= 233:
                        loc_alarm = 233
        
        elif state_machine == STM_SETOFFSET:
            if delta <> 0:
                loc_offset += delta * .1

            if temper_unit == TUN_CEL:
                if loc_offset > 10:
                    loc_offset = 10
                elif loc_offset <= -10:
                    loc_offset = -10
            elif temper_unit == TUN_FAR:
                if loc_offset > 50:
                    loc_offset = 50
                elif loc_offset <= 14:
                    loc_offset = 14
            elif temper_unit == TUN_KEV:
                if loc_offset > 283.15:
                    loc_offset = 283.15
                elif loc_offset <= 263.15:
                    loc_offset = 263.15
                    


##
# @fn mode_management
# @brief  Read the keypad 
#
def mode_management():
#
#  Act in base of te status of the mode button
#  If pressed AND released act
#
    global state_machine
    global alarmEnable
    global btnMode
    
    if btnMode == 1:
        # Alarm is running ! Disable buzzer if the button is pressed
        if state_machine == STM_RUN and alarmRun == True and alarmEnable == True:
            alarmEnable = False
            print "Disabled manually Alarm"
        else:   
            if state_machine == STM_RUN:
                # Change color display in Blue
                if LCD_RGB == 1:
                    lcd.set_color(0.0, 0.0, 1.0)

                lcd.clear()
                lcdString = "Set Unit\n                  "
                lcd.message(lcdString)
                state_machine = STM_SETUNIT
                print "Start configuration mode !"
                
            elif state_machine == STM_SETUNIT:
                # We reach here when we change mode from Set Unit.
                # At this point we update the alarm/offset values and save
                # the measurement unit
                convertValuesTemp() # Convert alarm and offset and update unit
                
                # No need to change color of the display
                lcd.clear()
                lcdString = "Enable Alarm \n                  "
                lcd.message(lcdString)
                state_machine = STM_ENALARM
                
            elif state_machine == STM_ENALARM:
                # No need to change color of the display
                
                # if the alarm is disabled, don't go on the setting area
                if alarmEnable == True:
                    state_machine = STM_SETALARM
                    lcdString = "Set alarm \n                  "
                else:
                    state_machine = STM_SETOFFSET
                    lcdString = "Set offset \n                  "

                lcd.clear()
                lcd.message(lcdString)
                    
            elif state_machine == STM_SETALARM:
                # No need to change color of the display
                lcd.clear()
                lcdString = "Set Offset \n                  "
                lcd.message(lcdString)
                state_machine = STM_SETOFFSET
                
            elif state_machine == STM_SETOFFSET:
                # No need to change color of the display
                lcd.clear()
                lcdString = "Reset ?    \n                  "
                lcd.message(lcdString)
                state_machine = STM_RESET

            elif state_machine == STM_RESET:
                if executeReset == True:
                    resetTeirmilab()
                    
                writeConfigFile()       # Save the parameters in a file         
                # Change color display in Green
                if LCD_RGB == 1:
                    lcd.set_color(0.0, 1.0, 0.0)
                lcd.clear()
                state_machine = STM_RUN
                print "End configuration mode !"
                
            else:
                # Change color display in Green
                if LCD_RGB == 1:
                    lcd.set_color(0.0, 1.0, 0.0)
                lcd.clear()
                state_machine = STM_RUN
                print "Forcing RUN !"

    
##
# @fn convertValuesTemp
# @brief Change the value of the temperatures settings 
# depending the measurement unit
#
def convertValuesTemp():
    global loc_alarm
    global loc_offset
    global tmp_temper_unit
    global temper_unit

    if temper_unit == tmp_temper_unit:
        exit
        
    if temper_unit == TUN_CEL and tmp_temper_unit == TUN_FAR:
        # Convert from Celsius to Fahrenheit
        # Multiply by 9, then divide by 5, then add 32
        # Debug
        print "Convert from C to F - C %f " % loc_alarm
        loc_calc   = ((loc_alarm * 9) / 5) +32
        loc_alarm  = loc_calc 
        # Debug
        print "Convert from C to F - F %f " % loc_alarm

    if temper_unit == TUN_CEL and tmp_temper_unit == TUN_KEV:
        # Convert from Celsius to Kelvin
        # Debug
        print "Convert from C to K - C %f " % loc_alarm
        loc_calc = loc_alarm + 273.15
        loc_alarm  = loc_calc 
        # Debug
        print "Convert from C to K - K %f " % loc_alarm

    if temper_unit == TUN_FAR and tmp_temper_unit == TUN_KEV:
        # Convert from Fahrenheit to Kelvin
        # (F - 32) * 5/9 + 273.15
        # Debug
        print "Convert from F to K - F %f " % loc_alarm
        loc_calc   = (((loc_alarm - 32) * 5) / 9) + 273.15
        loc_alarm  = loc_calc 
        # Debug
        print "Convert from F to K - K %f " % loc_alarm 

    if temper_unit == TUN_FAR and tmp_temper_unit == TUN_CEL:
        # Convert from Fahrenheit to Celsius
        # (F - 32) * 5/9
        # Debug
        print "Convert from F to C - F %f " % loc_alarm
        loc_calc   = ((loc_alarm - 32) * 5) / 9
        loc_alarm  = loc_calc 
        # Debug
        print "Convert from F to C - C %f " % loc_alarm
        
    if temper_unit == TUN_KEV and tmp_temper_unit == TUN_CEL:
        # Convert from Kelvin to Celsius
        # Debug
        print "Convert from K to C - K %f " % loc_alarm
        loc_calc = loc_alarm - 273.15
        loc_alarm  = loc_calc 
        # Debug
        print "Convert from K to C - C %f " % loc_alarm

    if temper_unit == TUN_KEV and tmp_temper_unit == TUN_FAR:
        # Convert from Kelvin to Fahrenheit
        # Subtract 273.15 then multiply by 9, then divide by 5, then add 32
        # Debug
        print "Convert from K to F - K %f " % loc_alarm
        loc_calc   = (((loc_alarm  - 273.15) * 9) / 5) +32
        loc_alarm  = loc_calc 
        # Debug
        print "Convert from K to F - F %f " % loc_alarm

    temper_unit = tmp_temper_unit

##
# @fn buzzer
# @brief  Generate noise with buzzer
#
def buzzer(test):
    
    if (alarmEnable == True and alarmRun == True) or test == True:
        count = 0                       # Buzzer test
        rGPIO.output(BUZZER, True)
        while (count < 20000):
            count = count + 1

        rGPIO.output(BUZZER, False)

        pygame.mixer.init()
        pygame.mixer.music.load("police_s.wav")
        if pygame.mixer.music.get_busy() == False:
            pygame.mixer.music.play()
            
#       while pygame.mixer.music.get_busy() == True:
#           continue


##
# @fn displayRun
# @brief  Display information during normal run
#
def displayRun():
    global loc_alarm
    global loc_offset
    global isAlive
    
    isAlive += 1    
    if isAlive > 1:
        isAlive = 0

    if temper_unit == TUN_CEL:
        try:
            temper = sensor.get_temperature() + loc_offset
        except:
            return
        
        if isAlive == 0:
            lcdString =  "Read : %02.2f C  \n" % (temper)
        else:   
            lcdString =  "Read : %02.2f C *\n" % (temper)
            
        if alarmEnable == True:
            lcdString += "Alarm: %02.2f C " % (loc_alarm)
        else:   
            lcdString += "Alarm: disabled "
    elif temper_unit == TUN_FAR:    
        try:
            temper = sensor.get_temperature(W1ThermSensor.DEGREES_F) + loc_offset
        except:
            return

        if isAlive == 0:
            lcdString =  "Read : %02.2f F  \n" % (temper)
        else:   
            lcdString =  "Read : %02.2f F *\n" % (temper)
        
        if alarmEnable == True:
            lcdString += "Alarm: %02.2f F  " % (loc_alarm)      
        else:   
            lcdString += "Alarm: disabled "
    else:
        try:
            temper = sensor.get_temperature(W1ThermSensor.KELVIN) + loc_offset
        except:
            return

        if isAlive == 0:
            lcdString =  "Read : %02.2f K  \n" % (temper)
        else:   
            lcdString =  "Read : %02.2f K *\n" % (temper)

        if alarmEnable == True:
            lcdString += "Alarm: %02.2f K  " % (loc_alarm)
        else:   
            lcdString += "Alarm: disabled "
        
    alarmManagement(temper)     # Evaluate if there is an alarm

    buzzer(False)                   # Notify an alarm with sound
    
    lcd.home()
    lcd.message(lcdString)

##
# @fn alarmManagement
# @brief The function handle the alarm management
# @parm readTemp    - temperature reading
#
def alarmManagement(readTemp):
    global loc_alarm
    global alarmRun
    
    if alarmEnable == True:
        if loc_alarm > 0:
            if alarmRun == False:
                # Change color display in RED if temperature > alarm otherwise keep green
                if readTemp > loc_alarm:
                    # Debug
                    print "Reached alarm condition ! Above %f " % loc_alarm
                    if LCD_RGB == 1:
                        lcd.set_color(1.0, 0.0, 0.0)
                    alarmRun = True
            else:
                if readTemp < loc_alarm:
                    # Debug
                    print "Restored alarm condition ! Below %f " % loc_alarm
                    if LCD_RGB == 1:
                        lcd.set_color(0.0, 1.0, 0.0)
                    alarmRun = False

        elif loc_alarm < 0:
            if alarmRun == False:
                # Change color display in BLUE if temperature < alarm otherwise keep green
                if readTemp < loc_alarm:
                    # Debug
                    print "Reached alarm condition ! Above %f " % loc_alarm
                    if LCD_RGB == 1:
                        lcd.set_color(0.0, 0.0, 1.0)
                    alarmRun = True
            else:
                if readTemp > loc_alarm:
                    # Debug
                    print "Restored alarm condition ! Below %f " % loc_alarm
                    if LCD_RGB == 1:
                        lcd.set_color(0.0, 1.0, 0.0)
                    alarmRun = False

        elif loc_alarm == 0:
            if alarmRun == False:
                # Change color display in BLUE if temperature < alarm otherwise keep green
                if readTemp == 0:
                    # Debug
                    print "Reached alarm condition ! Zero"
                    if LCD_RGB == 1:
                        lcd.set_color(0.0, 0.0, 1.0)
                    alarmRun = True
            else:
                if readTemp <> 0:
                    # Debug
                    print "Restored alarm condition ! Zero"
                    if LCD_RGB == 1:
                        lcd.set_color(0.0, 1.0, 0.0)
                    alarmRun = False
        

##
# @fn displayAlarmEnable
# @brief Display information when not run
#
def displayAlarmEnable():
    if alarmEnable == True:
        lcdString = "On  "
    else:
        lcdString = "Off "
        
    lcd.set_cursor(0, 1)
    lcd.message(lcdString)

##
# @fn displayAlarmSettings
# @brief Display information when not run
#
def displayAlarmSettings():
    global loc_alarm

    if temper_unit == TUN_CEL:
        lcdString = "Alarm: %02.2f C " % (loc_alarm)
    elif temper_unit == TUN_FAR:    
        lcdString = "Alarm: %02.2f F  " % (loc_alarm)
    else:
        lcdString = "Alarm: %02.2f K  " % (loc_alarm)
        
    lcd.set_cursor(0, 1)
    lcd.message(lcdString)


##
# @fn displayOffsetSettings
# @brief Display information when not run
#
def displayOffsetSettings():
    global loc_offset

    if temper_unit == TUN_CEL:
        lcdString = "Offset: %02.2f C " % (loc_offset)
    elif temper_unit == TUN_FAR:    
        lcdString = "Offset: %02.2f F  " % (loc_offset)
    else:
        lcdString = "Offset: %02.2f K  " % (loc_offset)
        
    lcd.set_cursor(0, 1)
    lcd.message(lcdString)

##
# @fn displayUnitSettings
# @brief Display information when not run
#
def displayUnitSettings():
    global tmp_temper_unit

    if tmp_temper_unit == TUN_CEL:
        lcdString = "Celsius   "
    elif tmp_temper_unit == TUN_FAR:
        lcdString = "Farheneit "
    else:   
        lcdString = "Kelvin    "
        
    lcd.set_cursor(0, 1)
    lcd.message(lcdString)

##
# @fn displayReset
# @brief Display information when not run
#
def displayReset():
    if executeReset == True:
        lcdString = "Yes "
    else:
        lcdString = "No  "
        
    lcd.set_cursor(0, 1)
    lcd.message(lcdString)

##
# @fn resetTeirmilab
# @brief  Reset the unit !
#
def resetTeirmilab():
    global loc_alarm
    global loc_offset
    global temper_unit
    global alarmEnable
    global executeReset

    print "RESET TeirmiLab"
    
    excuteReset = False # Remove reset command
    loc_alarm   = 0     # Value 0 for alarm
    loc_offset  = 0     # Value 0 for offset
    temper_unit = 0     # Celsius
    alarmEnable = False # Alarm diabled
    
    lcd.clear()
    lcd.message('Teirmilab RESET !\n                ')
    writeConfigFile()
    time.sleep(2.0)

##
# @fn readConfigFile
# @brief  Read the configuration file
#
def readConfigFile():
    global loc_alarm
    global loc_offset
    global temper_unit
    global alarmEnable

    print "Reading configuration file"
    
    if exists('teirmilab.conf') == True:
        confFile    = open('teirmilab.conf')
        loc_offset  = float(confFile.readline())
        loc_alarm   = float(confFile.readline())
        temper_unit = int(confFile.readline())
        alarmEnable = bool(confFile.readline())
        confFile.close()
    else:
        lcd.clear()
        lcd.message('No config file!\nCreating one')
        writeConfigFile()
        time.sleep(2.0)

##
# @fn writeConfigFile
# @brief  Write the configuration file
#
def writeConfigFile():
    global loc_alarm
    global loc_offset
    global temper_unit
    global alarmEnable

    confFile = open('teirmilab.conf', 'w')
    confFile.truncate() # Clean up the file
    confFile.write(str(loc_offset))
    confFile.write("\n")
    confFile.write(str(loc_alarm))
    confFile.write("\n")
    confFile.write(str(temper_unit))
    confFile.write("\n")
    confFile.write(str(alarmEnable))
    confFile.write("\n")
    confFile.close()


############################################################################
############################################################################
############    MAIN   #####################################################
############################################################################
############################################################################

# Define state machine states
STM_RUN         = 0
STM_SETUNIT    = 1
STM_ENALARM    = 2
STM_SETALARM    = 3
STM_SETOFFSET  = 4
STM_RESET      = 5

#define temperature values
TUN_CEL     = 0
TUN_FAR     = 1
TUN_KEV     = 2

#define Raspberry GPIO pins - use GPIO numbers here
SHUTDOWN_INPUT    = 17
SHUTDOWN_FEEDBACK = 18
BUZZER            = 22
ENCODER_SWITCH    = 25
ENCODER_A_PIN     = 23  
ENCODER_B_PIN     = 24

#define the type of display
# If 1 is RGB, if 0 is mono (select blue color)
LCD_RGB     = 1

# Define MCP pins connected to the LCD.
lcd_rs        = 0
lcd_en        = 1
lcd_d4        = 2
lcd_d5        = 3
lcd_d6        = 4
lcd_d7        = 5
lcd_red       = 6
lcd_green     = 7
lcd_blue      = 8

# Define MCP pins connected to the Keypad
key_1         = 12
key_2         = 13
key_3         = 14
key_4         = 15

# Define LCD column and row size for 16x2 LCD.
lcd_columns   = 16
lcd_rows      = 2
# Alternatively specify a 20x4 LCD.
# lcd_columns = 20
# lcd_rows    = 4

# Define variables for alarm and offset
loc_alarm     = 25.00
loc_offset    = 0.0
index_alarm   = 0
index_offset  = 0

alarmEnable   = False
alarmRun          = False

btnMode       = 0

executeReset  = False

isAlive       = 0                   # Counter to show char on LCD to indicate
                                        # the program is alive

#
# Initialize main state machine
state_machine   = STM_RUN       # Default display temperature
temper_unit     = TUN_CEL       # Default display in Celsius
tmp_temper_unit = TUN_CEL       # Round Counter for changing unit
                                        # 0 = Celsius
                                        # 1 = Fahrenheit
                                        # 2 = Kelvin

# Initialize MCP23017 device using its default 0x20 I2C address.
mGPIO = MCP.MCP23017()

# Alternatively you can initialize the MCP device on another I2C address or bus.
# mGPIO = MCP.MCP23017(0x24, busnum=1)

# Initialize the LCD using the pins 
lcd = LCD.Adafruit_RGBCharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, 
                            lcd_columns, lcd_rows, lcd_red, lcd_green, lcd_blue, gpio=mGPIO)

# Initialize the Keypad pins
mGPIO.setup(key_1, aGPIO.IN)
mGPIO.setup(key_2, aGPIO.IN)
mGPIO.setup(key_3, aGPIO.IN)
mGPIO.setup(key_4, aGPIO.IN)
# Enable pull up
mGPIO.pullup(key_1, 1)
mGPIO.pullup(key_2, 1)
mGPIO.pullup(key_3, 1)
mGPIO.pullup(key_4, 1)

#initialize other Raspberyy Pins
rGPIO.setmode(rGPIO.BCM)
rGPIO.setup(BUZZER, rGPIO.OUT)              # Buzzer
rGPIO.setup(SHUTDOWN_INPUT,  rGPIO.IN)      # Shutdown input
rGPIO.setup(SHUTDOWN_FEEDBACK, rGPIO.OUT)   # Shutdown feedback

rGPIO.setup(ENCODER_SWITCH, rGPIO.IN, pull_up_down=rGPIO.PUD_UP)    # Encoder switch

# Polling
#encoder = RotaryEncoder(ENCODER_A_PIN, ENCODER_B_PIN)

# Thread
encoder = RotaryEncoder.Worker(ENCODER_A_PIN, ENCODER_B_PIN)
encoder.start()

# Set up shutdown feedback high
rGPIO.output(SHUTDOWN_FEEDBACK, True)

# Print a two line message
lcd.message('   TeirmiLab    \n from TheFwGuy  ')

print "Start program !\nLoad 1Wire modules"

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
 
# Wait 2 seconds
time.sleep(2.0)

print "Verify if temperature sensor is present"

try:
    sensor = W1ThermSensor()    # Identify sensor
except:
    lcd.clear()
    lcd.message('NO SENSOR !     \nTurn off       ')
    while (1):
        read_shutdown() # Check shutdown signal
    
print "Sensor name : %s" % sensor.type_name

readConfigFile()                # Read stored values

#pygame.mixer.init()
#pygame.mixer.music.load("police_s.wav")

buzzer(True)        # Buzzer test 

# Set Green
if LCD_RGB == 1:
    lcd.set_color(0.0, 1.0, 0.0)
    
lcd.clear()

last_state = None

while (1):  
    read_switch()       # Read physically the encoder switch
    read_shutdown()     # Check shutdown signal

    mode_management()   # Handle selected mode

        
    if state_machine == STM_RUN:
        displayRun()
    elif state_machine == STM_SETUNIT:  
        read_encoder()  # Read encoder for values
        displayUnitSettings()
    elif state_machine == STM_ENALARM:  
        read_encoder()  # Read encoder for values
        displayAlarmEnable()
    elif state_machine == STM_SETALARM: 
        read_encoder()  # Read encoder for values
        displayAlarmSettings()
    elif state_machine == STM_SETOFFSET:    
        read_encoder()  # Read encoder for values
        displayOffsetSettings()
    elif state_machine == STM_RESET:    
        read_encoder()  # Read encoder for values
        displayReset()

#----------------------------------------------------------

lcd.clear()
lcd.message('Goodbye!')
# Turn backlight on.
lcd.set_backlight(0)

